var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var morgan = require('morgan');
var phantom = require("phantom");

var _ph, _page;
var renderwait = 100;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));

///////////////////////////////////////////////////7
 
app.get("/:page?", function(req, res) {
	
	var url = 'http://www.ucuracak.com/?_escaped_fragment_=';
	if(req.params.page) url+req.params.page;


    phantom.create(['--ignore-ssl-errors=yes', '--load-images=no']).then(ph => {
        _ph = ph;
        return _ph.createPage();
    }).then(function(page) {
        _page = page;
        _page.open(url).then(function(status) {
            console.log('URL: '+ url + ' STATUS:' + status);
            console.log('Waiting ' + renderwait / 1000 + ' seconds to render.');
 
                setTimeout(function() {
                    _page.property('content').then(function(content) {
                    	content='<base href="http://www.ucuracak.com" target="_blank">'+content;
                        res.send(content);
                        _page.close();
                        console.log('Page rendered and closed.');
                    })
                }, renderwait); 
        });

    }).catch(e => console.log(e));


});






/////////////////////////////////////////////////////

app.listen(process.env.PORT || 8080);
console.log('Magic happens');
//server starts
